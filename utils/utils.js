
const shuffleArray = (arr)=>{
    return  arr
        .map(value => ({ value, sort: Math.random() }))
        .sort((a, b) => a.sort - b.sort)
        .map(({ value }) => value)
}
const randomArray = (arr)=>{
    let toReturn = arr[0]
    arr.splice([0], 1)
    return toReturn
}

const createTile = (id , position  ,empty = false )=>{
    const tile = document.createElement('div')
    tile.classList.add('tile', 'border', 'border-black' , 'text-center' ,'items-center','cursor-pointer' , 'flex' , 'justify-center' ,'overflow-hidden')
    tile.setAttribute('data-position', position)
    tile.setAttribute('data-id', id)
    tile.innerHTML = id
    if(empty){
        tile.classList.add('empty')
    }
    return tile
}



export {shuffleArray , randomArray ,createTile}
